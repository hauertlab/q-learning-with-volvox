import matplotlib
import matplotlib.pyplot as plt
import glob
import numpy as np
import random
import datetime as dt
import copy

def get_state_from_index(index):
    frames_on = index%11
    index = index//11
    frames_off = index%11
    index = index // 11
    
    light_index = index
    frames_labels = ["0", "1", "2","3","4","5","6","7","8","9","10+"]
    light_labels = ["light on", "light off"]
    return ("frames on : " +
            frames_labels[frames_on], "frames off : " + frames_labels[frames_off], light_labels[light_index])


def plot_numpy_array(filename):
    fig, ax = plt.subplots()

    array = np.load(filename)

    nzrows, nzcols = array.nonzero()
    nzrows = np.unique(nzrows)
    smaller_array = array[nzrows, :]

    ax.matshow(smaller_array, cmap=plt.cm.Blues)

    ytick_pos = list(range(len(smaller_array)))
    labels = []
    for value in nzrows:
        labels.append(get_state_from_index(value))
    plt.yticks(ytick_pos, labels)

    for i in range(smaller_array.shape[1]):
        for j in range(smaller_array.shape[0]):
            c = smaller_array[j, i]
            ax.text(
                i,
                j,
                f"{c:.2f}",
                va='center',
                ha='center',
                fontsize='xx-small')
    plt.title(filename)


def plot_diff(filename):
    fig, ax = plt.subplots()
    y_vec = []
    x_vec = []
    with open(filename) as f:
        for line in f:
            time_str, diff_str = line.split(",")
            datetime_object = dt.datetime.strptime(time_str, '%H:%M: %S')
            x_vec.append(datetime_object)
            y_vec.append(float(diff_str))

    ax.plot(x_vec, y_vec)
    plt.gcf().autofmt_xdate()

    # Text in the x axis will be displayed in 'YYYY-mm' format.
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M:%S'))

    if "speed" in filename:
        plt.ylabel("Speed (px/frame)")
        plt.title("Speed")
    elif "sum" in filename:
        plt.ylabel("sum(Q)")
        plt.title("Q-table sum")
    else:
        plt.ylabel("Diff")
        plt.title("Q-Learning diff")
    plt.xlabel("Time")


def plot_actions(filename):
    fig = plt.figure()
    ax = fig.add_subplot(211)
    array = np.load(filename)
    ax.plot(array)
    plt.xlabel("Time")
    plt.ylabel("Light status")
    ax2 = fig.add_subplot(212)

    # Measure the frequency of change
    change_points = []
    previous_value = None
    counter = 0

    final_array = array
    first_action = final_array[0]
    for value in final_array:
        if value != previous_value:
            change_points.append(counter)
            previous_value = value
            counter = 0
        counter += 1
    
    # Measure the stability of points
    frequency_of_pairs_of_values = {}
    for i in range(0, len(change_points) - 1, 2):
        current_key = repr([change_points[i], change_points[i + 1]])
        if current_key in frequency_of_pairs_of_values:
            frequency_of_pairs_of_values[current_key] += 1
        else:
            frequency_of_pairs_of_values[current_key] = 1

    try:
        max_key = max(frequency_of_pairs_of_values,
                  key=lambda k: frequency_of_pairs_of_values[k])
        max_frequency = frequency_of_pairs_of_values[max_key]
        stability_score = float(max_frequency) / (len(change_points) / 2)
        print(
        f"Stability score is {stability_score} for values {max_key} (first value is {first_action})")

        plt.bar(range(len(frequency_of_pairs_of_values)), list(
            frequency_of_pairs_of_values.values()), align='center')
        plt.xticks(
            range(
                len(frequency_of_pairs_of_values)), list(
                frequency_of_pairs_of_values.keys()))
    except ValueError:
        print(f"Could not plot action frequency of {change_points}")


if __name__ == "__main__":
    for filename in glob.glob('output*.csv'):
        plot_diff(filename)

    for filename in glob.glob('output*.npy'):
        plot_numpy_array(filename)

    for filename in glob.glob('actions*.npy'):
        plot_actions(filename)

    plt.show()
