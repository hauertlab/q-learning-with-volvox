
import enum
import numpy as np
import random
import math
import logging
import learning
from learning import Action, Behaviour
import datetime


class VolvoxLearningModel():

    def __init__(self, output_files=True):

        light_on_states = 11
        light_off_states = 11
        self.__observation_space_n = light_on_states * light_off_states * 2
        self.__action_space_n = 2  # on/off
        # Initialize Q - table with zeros
        self.__q_table = np.zeros(
            [self.__observation_space_n, self.__action_space_n])
        self.try_load_qtable()
        self.__episode = 0
        self.__gamma = 0.3
        self.__alpha = 0.2

        self.__max_epsilon = 1
        self.__min_epsilon = 0.01
        self.__epsilon = self.__max_epsilon
        self.__decay = 0.01

        self.__last_action = None  # will be of type Action
        self.__last_state = None  # will be an index
        self.__behaviour = Behaviour.LEARN_STOP
        self.__last_action_list = np.array([])
        self.__prior_reward = 0
        self.__output_files = output_files

        # Saving
        now = datetime.datetime.now()
        self.filename = 'output-' + str(now.minute) + ":" + str(now.second)
        self.filename_npy = self.filename + ".npy"
        self.filename_csv = self.filename + "sum.csv"
        self.filename_speed = self.filename + "speed.csv"
        self.filename_actions = "actions" + \
            str(now.minute) + ":" + str(now.second) + ".npy"

    def try_load_qtable(self):
        try:
            array = np.load("output.npy")
            if array.shape == (self.__observation_space_n,
                               self.__action_space_n):
                self.__q_table = array
                logging.info("Loaded q table array")
            return

        except BaseException:
            return

    def step(self, history, frames_off, frames_on, light_status):
        self.__episode += 1
        state_index = learning.get_state_index(
            history, frames_on, frames_off, light_status)

        velocity = 0
        acceleration = 0
        reward = 0

        if len(history) > 0:
            last_speed = history[-1]
            if last_speed is not None:
                velocity = last_speed.velocity
                acceleration = last_speed.acceleration
        if self.__last_action is not None and self.__last_state is not None:
            # Update Q matrix with the result
            reward = learning.get_state_reward(
                self.__behaviour, velocity, acceleration, frames_on, frames_off)
            new_q_value = (1 - self.__alpha) * self.__q_table[self.__last_state, self.__last_action.value] \
                + self.__alpha * (reward + self.__gamma *
                                  np.max(self.__q_table[state_index]))

            self.__q_table[self.__last_state,
                           self.__last_action.value] = new_q_value
            sumQ = np.sum(self.__q_table)

            if self.__output_files:
                now = datetime.datetime.now()
                with open(self.filename_speed, 'a') as f:
                    f.write(f"{now.hour}:{now.minute}: {now.second}")
                    f.write(",")
                    f.write(str(abs(velocity)))
                    f.write("\n")

                with open(self.filename_csv, 'a') as f:
                    f.write(f"{now.hour}:{now.minute}: {now.second}")
                    f.write(",")
                    f.write(str(sumQ))
                    f.write("\n")

                np.save(self.filename_npy, self.__q_table)
                np.save(self.filename_actions, self.__last_action_list)

        next_action = self.choose_action(state_index)
        self.__last_action = next_action
        self.__last_state = state_index
        # Only keep the last 1000 actions
        self.__last_action_list = np.append(
            self.__last_action_list, next_action.value)
        self.__last_action_list = self.__last_action_list[-1000:]
        # Decrease epsilon progressively
        # Only if we are increasing the reward
        if self.__episode > 100 and reward > self.__prior_reward:
            self.__epsilon = self.__min_epsilon + \
                (self.__max_epsilon - self.__min_epsilon) * \
                np.exp(-self.__decay * (self.__episode - 100))

        self.__prior_reward = reward
        return self.__last_action

    def choose_action(self, state):
        # Exploration
        if random.uniform(0, 1) < self.__epsilon:
            return Action(random.randint(0, self.__action_space_n - 1))
        # Exploitation
        elif state < self.__q_table.shape[0]:
            # If both values are equal, chose the last action
            if self.__q_table[state][0] == self.__q_table[state][1]:
                return Action(random.randint(0, 1))
            return Action(np.argmax(self.__q_table[state]))
        else:
            logging.error(f"invalid state {state}")
            return Action.LIGHT_OFF

    def q_table(self):
        # Readable Q-table
        return self.__q_table

    def has_learned(self):
        return self.__epsilon < self.__min_epsilon * 2

    def set_do_learning(self, value):
        return False
