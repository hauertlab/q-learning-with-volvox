import logging
from geometry import Point, Rectangle
import math
import random


class CellAccumulator():
    def __init__(self, frames_on, frames_off):

        self.__max = 20
        self.__min = 1
        self.__light_value = self.__min
        self.__dark_value = self.__min
        self.__lambda_1 = math.ceil(
            1000 * math.log(self.__max / self.__min) * (1 / frames_on)) / 1000
        self.__lambda_2 = math.ceil(
            1000 * math.log(self.__max / self.__min) * (1 / frames_off)) / 1000
        self.__time = 0
        self.__last_state = None

    def is_saturated_light(self):
        return self.__light_value >= self.__max

    def is_saturated_dark(self):
        return self.__dark_value >= self.__max

    def new_frame(self, light_on: bool):
        if light_on != self.__last_state:
            self.__time = 0
            self.__last_state = light_on
            if light_on and self.__dark_value >= self.__max / \
                    math.exp(self.__lambda_2):
                self.__light_value = self.__min
            elif not light_on and self.__light_value >= self.__max / math.exp(self.__lambda_1):
                self.__dark_value = self.__min

        else:
            self.__time += 1
        if light_on and not self.is_saturated_light():
            # Increase
            self.__light_value = self.__min * \
                math.exp(self.__lambda_1 * self.__time)
            # clamp
            self.__light_value = min(
                max(self.__min, self.__light_value), self.__max)
        if not light_on and not self.is_saturated_dark():
            # Decay
            self.__dark_value = self.__min * \
                math.exp(self.__lambda_2 * self.__time)
            # clamp
            self.__dark_value = min(
                max(self.__min, self.__dark_value), self.__max)


class Agent():
    def __init__(
            self,
            arena: Rectangle,
            speed: float,
            theta: float,
            pos: Point,
            frames_on: int,
            frames_off: int):
        """
        When the light is turned on, it will stop immediately, then after
        frames_on it will habituate

        When the light is off, it will start moving, but will not stop with
        light until after #frames_off

        """
        self.__base_speed = speed
        self.__current_speed = speed
        self.__current_position = pos
        self.__direction_theta = theta
        self.__arena_rect = arena
        self.__acceleration = 0
        self.__radius = random.randint(5, 15)
        logging.debug(f"Agent created {frames_on}, {frames_off}")

        self.__cell = CellAccumulator(frames_on, frames_off)

    def position(self):
        return self.__current_position

    def rebound(self):
        self.__direction_theta = self.__arena_rect.rebound(
            self.__current_position, self.__direction_theta)

    def speed(self):
        return self.__current_speed

    def update(self, light_on_and_orientation):
        light_on, orientation = light_on_and_orientation
        self.__cell.new_frame(light_on)
        self.__current_speed = max(
            0, self.__current_speed + self.__acceleration)
        # Random motion
        if random.random() < 0.01:
            self.__direction_theta = 2 * math.pi * random.random()

        self.__current_position.update(
            self.__current_speed, self.__direction_theta)
        if not self.__arena_rect.contains(self.__current_position):
            self.rebound()
        if light_on and orientation is None:
            if self.__cell.is_saturated_light():
                # There's been light for a while
                self.__current_speed = self.__base_speed
                logging.debug("Saturated light")
            else:

                if self.__cell.is_saturated_dark():
                    # We're zapping
                    #acc_fraction =1/(self.__frames_on)
                    #self.__acceleration = -acc_fraction * self.__base_speed
                    self.__current_speed = 0

        elif light_on and orientation is not None:
            self.__direction_theta += (15 * math.pi / 180.0)

        else:
            if self.__cell.is_saturated_dark():
                # if it had stopped and then it goes dark it will
                # continue moving after habituating
                #self.__acceleration = acc_fraction * self.__base_speed
                self.__current_speed = self.__base_speed
                logging.debug("Saturated dark")

    def radius(self):
        return self.__radius
