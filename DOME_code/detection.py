from random import random
from learning import Action, Behaviour
from geometry import Point
import base_learning as learning
import logging
import math


class TimepointInformation():
    def __init__(self, position, instant_velocity, avg_velocity, theta, acceleration,
                 time_difference, counter):
        self.position = position
        self.velocity = avg_velocity  # pixels per microsecond
        self.instant_velocity = instant_velocity
        self.direction = theta
        self.acceleration = acceleration
        self.dt = time_difference
        self.global_counter = counter

    def to_list(self):
        return [self.position, self.velocity, self.instant_velocity,
                self.direction, self.acceleration, self.dt, self.global_counter]


class Detection:
    def __init__(self, x, y, w, h):
        self.pos = Point(x, y)
        self.width = w
        self.height = h

    def position(self):
        return self.pos


class TrackedDetection:
    def __init__(self, detection: Detection, output_files=True):
        self.__detection = detection

        # is either 0 or 1, with 0 meaning it is not detected in a given frame
        # and 1 meaning that it is.
        self.__detected_in_current_frame = 1

        # the summation of this over subsequent frames,
        # so if the agent is active and continues to be it will be incremented by 0.
        # If the agent is lost and becomes deactive, then this will become
        # increasingly negative over time.
        self.__detections_last_frames = 0

        self.__history = []
        self.__learning = learning.VolvoxLearningModel(output_files)

        self.frames_light_on = 0
        # Assume no light happened for a while in the past
        self.frames_light_off = 100
        self.current_light_status = Action.LIGHT_OFF
        # Value, in degrees, of the semicircle drawn on top of the direction
        # it will be pushed at 90 degrees of this direction
        # if elipse_direction = 0, the illuminated semicircle will go from 0 to -180
        # if elipse_direction = 180, the illuminated semicircle will go from 0 to 180
        # If it is none, a full circle will be drawn
        self.elipse_direction = None

        # If it is part of a trail
        self.__in_trail = False
        self.__is_emitting = False
        logging.debug("New detection found")

    def position(self):
        return self.__detection.pos

    def width(self):
        return self.__detection.width

    def height(self):
        return self.__detection.height

    def predict_action(self, behaviour, desired_direction=None):
        action = None

        if behaviour == learning.Behaviour.LEARN_STOP:
            action = self.__learning.step(
                self.__history,
                self.frames_light_off,
                self.frames_light_on,
                self.current_light_status.value)
        elif behaviour == learning.Behaviour.LIGHT_ON_ALWAYS:
            action = Action(1)
        elif behaviour == learning.Behaviour.LIGHT_OFF_ALWAYS:
            action = Action(0)
        elif behaviour == learning.Behaviour.LEARN_DIRECTION:
            if len(self.__history) > 0:
                current_direction = self.__history[-1].direction
                # assuming both are between 0 and 2pi
                if current_direction < 0:
                    current_direction += 2 * math.pi
                elif current_direction >= 2 * math.pi:
                    current_direction -= 2 * math.pi

                if abs(current_direction - desired_direction) > 10 * \
                        math.pi / 180:
                    self.elipse_direction = desired_direction * 180 / math.pi
                    action = Action(1)
                else:
                    self.elipse_direction = None
                    action = Action(0)
        elif behaviour == learning.Behaviour.STOP:
            self.__learning.set_do_learning(False)
            action = self.__learning.step(
                self.__history,
                self.frames_light_off,
                self.frames_light_on,
                self.current_light_status.value)
        elif behaviour == learning.Behaviour.TRAIL:
            if self.is_in_trail():
                self.__learning.set_do_learning(False)
                action = self.__learning.step(
                    self.__history,
                    self.frames_light_off,
                    self.frames_light_on,
                    self.current_light_status.value)
            else:
                return Action(0)
        elif behaviour == learning.Behaviour.STOP_HARDCODED:
            if self.current_light_status == Action.LIGHT_ON:
                if self.frames_light_on < 5:
                    action = Action.LIGHT_ON
                else:
                    action = Action.LIGHT_OFF
            else:
                if self.frames_light_off < 7:
                    action = Action.LIGHT_OFF
                else:
                    action = Action.LIGHT_ON
        else:
            action = Action.LIGHT_ON

        if self.current_light_status == action:
            if action == learning.Action.LIGHT_ON:
                self.frames_light_on += 1
            else:
                self.frames_light_off += 1
        else:
            if action == learning.Action.LIGHT_ON:
                self.frames_light_on = 1
            else:
                self.frames_light_off = 1
        self.current_light_status = action

        return action

    def to_list(self, include_history=False, include_learning=False):
        detection_list = [
            self.__detection.pos.x(),
            self.__detection.pos.y(),
            self.__detection.width,
            self.__detection.height,
            self.__detected_in_current_frame,
            self.__detections_last_frames,
            self.elipse_direction]
        if include_history:
            detection_list.append(self.__history)
        if include_learning:
            detection_list.append(self.__learning.q_table())
        return detection_list

    def update_deactivity(self):
        self.__detections_last_frames = self.__detections_last_frames + \
            (self.__detected_in_current_frame - 1)

    def update(self, new_detection, time_difference_ms, global_counter):
        current_pos = new_detection.position()
        past_pos = self.position()
        # Given the new detection, update all the characteristics
        velocity = current_pos.distance(past_pos) / time_difference_ms
        velocity_theta = past_pos.direction(current_pos)
        acceleration = velocity / time_difference_ms

        last_timepoint = self.last_velocity_information()
        if last_timepoint is not None:
            past_velocity = last_timepoint.velocity
            acceleration = (velocity - past_velocity) / time_difference_ms

        self.__detection = new_detection
        self.__detected_in_current_frame = 1
        self.__detections_last_frames = 0
        self.add_velocity_information(
            velocity,
            velocity_theta,
            acceleration,
            time_difference_ms,
            global_counter)

    def reset_deactivity(self):
        self.__detected_in_current_frame = 0

    def is_active(self, threshold=3):
        return self.__detections_last_frames > -threshold

    def detected(self):
        return self.__detected_in_current_frame

    def distance(self, other_detection):
        return self.position().distance(other_detection.position())

    def __eq__(self, other):
        return self.position() == other.position(
        ) and self.__detected_in_current_frame == other.__detected_in_current_frame and self.__detections_last_frames == other.__detections_last_frames

    def add_velocity_information(
            self, instant_velocity, theta, acceleration, time_difference, global_counter):

        self.__history.append(
            TimepointInformation(self.position(), instant_velocity, self.calculate_average_velocity(), theta, acceleration, time_difference, global_counter))

    def calculate_average_velocity(self):
        if len(self.__history) > 0:
            start_index = max(len(self.__history) - 10, 0)
            total_time = 0
            start_position = self.__history[start_index].position
            for i in range(start_index, len(self.__history)):
                timepoint = self.__history[i]
                total_time += timepoint.dt
            end_position = self.position()
            return end_position.distance(start_position) / total_time

        return 0

    def last_velocity_information(self):
        if len(self.__history) == 0:
            return None
        else:
            return self.__history[-1]

    def matching_score(self, detection):
        return detection.position().manhattan_distance(self.position())

    def has_learned(self):
        return self.__learning.has_learned()

    def emit(self, emit_value):
        self.__is_emitting = emit_value

    def is_emitting(self):
        return self.__is_emitting

    def is_in_trail(self):
        return self.__in_trail

    def add_to_trail(self):
        self.__in_trail = True
