import cv2
import pandas
import enum
import numpy as np
import os


class Source(enum.Enum):
    FILE = 1
    EMULATED = 2
    HARDWARE = 3


# For volvox specifically, (0,0,50) is dark red and doesn't interfere with the other wavelengths,
# while allowing to see the volvox
PROJECTION_BACKGROUND = (0, 0, 50)
PROJECTION_COLOUR = (75, 75, 0)
DEBUG = False


def is_on_raspberry():
    return "arm" in os.uname().machine


def write_to_file(agent_list, filename):
    df = pandas.DataFrame(agent_list)
    # Write to csv in append mode
    df.to_csv(filename, mode='a')


def draw_circle(img, position, radius, colour, width, angle=None):
    (cx, cy) = position
    halo_size = int(round(radius * 1.5))
    if angle is None:
        cv2.circle(img, (cx, cy), halo_size, colour, width)
    else:
        # The ellipse at 0 degrees will be a half circle (u-shaped)
        # The ellipse at 180 degrees will be a half circle (n-shaped)
        cv2.ellipse(img, (cx, cy), (halo_size, halo_size),
                    angle, 0, 180, colour, width)


def draw_circles(img, agent_list, colour=(75, 75, 0), width=-1):
    for points in agent_list:
        draw_circle(img, (points[0], points[1]),
                    points[3], colour, width, points[-1])


def write_text(img, text, position, colour=(0, 0, 0)):
    cv2.putText(img, text, position, cv2.FONT_HERSHEY_SIMPLEX, 0.3, colour)


def load_and_rescale(filepath, desired_img_shape):
    try:
        img = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
        if img is not None:
            return cv2.resize(img, desired_img_shape)
    except FileNotFoundError:
        return None
    return None
