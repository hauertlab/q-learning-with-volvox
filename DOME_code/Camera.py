import csv
import cv2
import glob
import logging
import numpy as np
import os
import time
import Utils

if Utils.is_on_raspberry():
    from picamera.array import PiRGBArray
    from picamera import PiCamera, PiCameraError


class Camera():
    def __init__(self):
        self.dx_cam = 0
        self.dy_cam = 0
        self.theta = 0
        self.stretch_x = 0
        self.stretch_y = 0
        self.dx_proj = 0
        self.dy_proj = 0
        self.center_point = (0, 0)
        self.center_size = (0, 0)

    def __init__(self, calibration_file, central_pixel_file):
        self.load(calibration_file)
        self.load_central_pixel(central_pixel_file)

    def load(self, file):
        with open(file) as f:
            csvreader = csv.reader(f, delimiter=',', quotechar=None)
            row = next(csvreader)
            logging.debug(row)
            self.dx_cam = float(row[0])
            self.dy_cam = float(row[1])
            self.theta = float(row[2])
            self.stretch_x = float(row[3])
            self.stretch_y = float(row[4])
            self.dx_proj = float(row[5])
            self.dy_proj = float(row[6])

    def load_central_pixel(self, file):
        with open(file) as f:
            csvreader = csv.reader(f, delimiter=',', quotechar=None)
            row = next(csvreader)
            logging.debug(row)
            center_row = int(row[0])
            center_column = int(row[1])
            row_length = int(row[2])
            column_length = int(row[3])
            self.center_point = (center_row, center_column)
            self.center_size = (row_length, column_length)
            logging.info(
                f"Center region is {self.center_point} with size {self.center_size}")

    def translate(self, rect, dx, dy):
        r2 = rect.copy()
        r2[0, :] = rect[0, :] + dx
        r2[1, :] = rect[1, :] + dy
        return r2

    def size(self):
        return (self.center_size[0] * 2, self.center_size[1] * 2)

    def coordinateTransform(self, coordinate):
        # transform camera coordinates into projector space
        coordinate = np.array(coordinate, dtype=np.int64)
        cameraCoordinate = np.reshape(coordinate, (2, 1))
        rotation_matrix = np.array([
            [np.cos(self.theta), np.sin(self.theta)],
            [-np.sin(self.theta), np.cos(self.theta)]
        ])

        stretch_matrix = np.array([
            [(self.stretch_x), 0],
            [0, (self.stretch_y)]
        ])
        translated = self.translate(
            cameraCoordinate, -self.dx_cam, -self.dy_cam)
        rotated = np.matmul(rotation_matrix, translated)
        stretched = np.matmul(stretch_matrix, rotated)
        translatedFinal = self.translate(stretched, self.dx_proj, self.dy_proj)
        translatedCoordinate = [
            int(round(translatedFinal[0][0])), int(round(translatedFinal[1][0]))]
        return translatedCoordinate

    def radiusTransform(self, rx, ry):
        return (rx * self.stretch_x, ry * self.stretch_y)

    def crop_frame(self, frame):
        top_left_row = max(
            0, self.center_point[0] - (self.center_size[0]))
        bot_right_row = min(
            frame.shape[1], self.center_point[0] + (self.center_size[0]))
        top_left_col = max(
            0, self.center_point[1] - (self.center_size[1]))
        bot_right_col = min(
            frame.shape[0], self.center_point[1] + (self.center_size[1]))
        return frame[top_left_col:bot_right_col, top_left_row:bot_right_row]

    def top_left_value(self):
        top_left_row = max(
            0, self.center_point[0] - (self.center_size[0]))
        top_left_col = max(
            0, self.center_point[1] - (self.center_size[1]))
        return(top_left_row, top_left_col)


class EmulatedCamera(Camera):
    def __init__(self, calibration_file, pixel_file, img_location):
        super().__init__(calibration_file, pixel_file)
        self.__file_iter = iter(
            sorted(
                glob.glob(
                    img_location +
                    '/*.png'),
                key=os.path.basename))

    def next_frame(self):
        file = next(self.__file_iter)
        return cv2.imread(file)

    def crop_frame(self, frame):
        return frame


class DOMECamera(Camera):
    def __init__(self, calibration_file, pixel_file):
        super().__init__(calibration_file, pixel_file)
        resolution = (1920, 1088)
        self.__picam = None
        try:
            self.__picam = PiCamera()
            self.__picam.resolution = (resolution)
            self.__picam.hflip = True
            #self.__picam.iso = 200
            self.__picam.shutter_speed = 50000
            self.__picam.exposure_mode = "spotlight"
            self.__raw_capture = PiRGBArray(self.__picam, size=resolution)
            self.__continuous = self.__picam.capture_continuous(
                self.__raw_capture, format='bgr', use_video_port=True)
        except PiCameraError:
            logging.error("Failed to initialise camera")

    def next_frame(self):
        if self.__picam:
            start = time.time()
            frame = next(self.__continuous)
            acq_time = time.time() - start
            logging.debug(f"Acquisition took {acq_time}")
            self.__raw_capture.truncate(0)
            return np.array(frame.array)
        else:
            return None
