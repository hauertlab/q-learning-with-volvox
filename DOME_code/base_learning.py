
import enum
import numpy as np
import random
import math
import logging
import learning
from learning import Action, Behaviour
import datetime


class State(enum.Enum):
    CLEAR = 1
    LEARN_ON = 2
    DO_ON = 3
    LEARN_OFF = 4
    EXPLOITATION = 5


class VolvoxLearningModel():
    def __init__(self, output_files=True):

        self.__state = State.CLEAR
        self.__behaviour = Behaviour.LEARN_STOP
        self.__output_files = output_files
        self.__episode = 0
        self.MAX_EPISODES = 10
        self.COOLDOWN_FRAMES = 10
        self.MIN_VELOCITY = 0.02
        self.MIN_AVG_VELOCITY = 0.02
        self.__learnt_on = None
        self.__learnt_off = None
        self.__episode_exploit_start = None

        self.__do_learning = True
        # Metric to check if explotaition is good or if we should restart learning
        # This will allow to not re-learn everything if there is a sudden
        # movement
        self.__last_speeds = []
        # Saving
        now = datetime.datetime.now()
        self.filename = 'output-' + \
            str(now.minute) + ":" + str(now.second) + str(now.microsecond)
        self.filename_npy = self.filename + ".npy"
        self.filename_csv = self.filename + "sum.csv"
        self.filename_speed = self.filename + "speed.csv"
        self.filename_actions = "actions" + \
            str(now.minute) + ":" + str(now.second) + ".npy"

    def go_to_exploit(self):
        self.__state = State.EXPLOITATION
        self.__episode_exploit_start = self.__episode + 1
        self.__last_speeds = []

    def step(self, history, frames_off, frames_on, light_status):
        velocity = 0
        acceleration = 0
        reward = 0
        avg_velocity = 0
        self.__episode += 1
        if len(history) > 0:
            last_speed = history[-1]
            if last_speed is not None:
                velocity = last_speed.instant_velocity
                avg_velocity = last_speed.velocity

                acceleration = last_speed.acceleration

            if self.__output_files:
                now = datetime.datetime.now()
                with open(self.filename_speed, 'a') as f:
                    f.write(datetime.datetime.strftime(now, "%H:%M:%S-%f"))
                    f.write(",")
                    f.write(str(abs(velocity)))
                    f.write("\n")

        if self.__state == State.CLEAR:
            self.__last_speeds.append(abs(velocity))
            self.__last_speeds = self.__last_speeds[-10:]
            if light_status == 0 and self.__episode >= self.COOLDOWN_FRAMES:
                self.__state = State.LEARN_ON
                self.MIN_VELOCITY = avg_velocity / 2
            return Action(0)
        elif self.__state == State.LEARN_ON:
            if light_status == 0:
                if abs(velocity) <= self.MIN_VELOCITY:
                    self.__learnt_on = 0
                    self.__learnt_off = float('inf')
                    self.go_to_exploit()
                    return Action(0)
                return Action(1)
            else:  # light_status == 1:
                if abs(velocity) <= self.MIN_VELOCITY:
                    self.__learnt_on = frames_on
                if abs(velocity) > self.MIN_VELOCITY and self.__learnt_on is not None:
                    self.__state = State.DO_ON
                    self.__learnt_on = max(1, self.__learnt_on - 1)
                    return Action(0)
                return Action(1)

        elif self.__state == State.DO_ON:
            if light_status == 0:
                if frames_off < self.COOLDOWN_FRAMES:
                    return Action(0)
                else:
                    return Action(1)
            else:  # light_status == 1
                if frames_on < self.__learnt_on:
                    return Action(1)
                else:
                    self.__state = State.LEARN_OFF
                    return Action(0)

        elif self.__state == State.LEARN_OFF:
            if light_status == 0:
                if abs(velocity) <= self.MIN_VELOCITY:
                    self.__learnt_off = frames_off

                if abs(velocity) > self.MIN_VELOCITY and self.__learnt_off is not None:
                    self.go_to_exploit()
                    logging.debug(
                        f"Learned values {self.__learnt_on}, {self.__learnt_off}")
            return Action(0)

        elif self.__state == State.EXPLOITATION:
            if self.__episode - self.__episode_exploit_start >= 20:
                self.__last_speeds.append(abs(velocity))
                if self.__do_learning and len(self.__last_speeds) > 10:
                    self.__last_speeds = self.__last_speeds[-10:]
                    movements = sum(
                        map(lambda x: x > self.MIN_AVG_VELOCITY, self.__last_speeds))

                    if movements >= 3:
                        self.__state = State.CLEAR
                        self.__last_speeds = []
                        logging.info(
                            f"Learnt {self.__learnt_on, self.__learnt_off} but moving at {abs(velocity)}. Restarting...")
                        self.__learnt_off = None
                        self.__learnt_on = None
                        self.__episode = 0

                        return Action(0)

            if light_status == 0:
                if frames_off < self.__learnt_off:
                    return Action(0)
                else:
                    return Action(1)
            else:
                if frames_on < self.__learnt_on:
                    return Action(1)
                else:
                    return Action(0)
        else:
            logging.error(f"Invalid state {self.__state}")
            return Action(0)

    def q_table(self):
        # Readable Q-table
        return np.ndarray([])

    def has_learned(self):
        return self.__state == State.EXPLOITATION

    def set_do_learning(self, do_learning):
        self.__do_learning = do_learning
