import logging
import math
import random


class Point():
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    def update(self, speed, theta):
        self.__x = self.__x + speed * math.cos(theta)
        self.__y = self.__y + speed * math.sin(theta)

    def x(self):
        return self.__x

    def y(self):
        return self.__y

    def __str__(self):
        return f"({self.__x},{self.__y})"

    def __repr__(self):
        return f"({self.__x},{self.__y})"

    def manhattan_distance(self, other):
        return abs(float(self.x()) - other.x()) + \
            abs(float(self.y()) - other.y())

    def distance(self, other):
        return math.sqrt((float(self.x()) - float(other.x()))**2 +
                         (float(self.y()) - float(other.y()))**2)

    def direction(self, other):
        # Returns the angle (rad) of the vector from self to go to other
        dy = float(other.y()) - float(self.y())
        dx = float(other.x()) - float(self.x())
        return math.atan2(dy, dx)

    def __add__(self, other):
        return Point(self.__x + other.x(), self.__y + other.y())


class Rectangle():
    def __init__(self, x, y, w, h):
        self.__top_left = Point(x, y)
        self.__width = w
        self.__height = h

    def contains(self, point: Point):
        return self.__top_left.x() <= point.x() < self.__top_left.x() + \
            self.__width and self.__top_left.y() <= point.y(
        ) < self.__top_left.y() + self.__height

    def rebound(self, pose: Point, current_theta: float):
        # Clamping
        new_theta = current_theta
        if new_theta < 0:
            new_theta += 2 * math.pi
        elif new_theta >= 2 * math.pi:
            new_theta -= 2 * math.pi

        # Exit through vertical walls
        if pose.x() <= self.__top_left.x() and math.pi / \
                2 <= new_theta <= 3 * math.pi / 2:
            new_theta = math.pi - current_theta

        if pose.x() >= self.__top_left.x() + self.__width and (new_theta <=
                                                               math.pi / 2 or new_theta >= 3 * math.pi / 2):
            new_theta = math.pi - current_theta

        # Exit through the bottom
        if pose.y() <= self.__top_left.y() and new_theta >= math.pi:
            new_theta = 2 * math.pi - new_theta
        # exit through the top
        if pose.y() >= self.__top_left.y() + self.__height and 0 <= new_theta <= math.pi:
            new_theta = 2 * math.pi - new_theta
        return new_theta

    def random_point(self, seed):
        x = random.uniform(
            self.__top_left.x(),
            self.__top_left.x() +
            self.__width)
        y = random.uniform(
            self.__top_left.y(),
            self.__top_left.y() +
            self.__height)
        return Point(x, y)
