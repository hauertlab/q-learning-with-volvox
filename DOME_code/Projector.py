import socket
import time
import cv2
import numpy as np
import json
import random
import logging

import Utils


class Projector():
    def __init__(self, ip, port, width=854,
                 height=480, full_screen=False):
        # Pattern
        self.display_width = width
        self.display_height = height
        # create a zero array of size specified above with data type 8-bit
        # unsigned integer
        self.pattern = np.zeros(
            (self.display_height, self.display_width, 3), dtype=np.uint8)
        self.pattern[:] = Utils.PROJECTION_BACKGROUND
        self.rectangle_top_corner = None
        self.rectangle_bot_corner = None
        self.running = False
        self.__debug = False

        # Server socket
        self.serversocket = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.serversocket.connect((ip, port))
        except ConnectionRefusedError as e:
            logging.error("Could not connect to the socket - aborting")
            raise e

        # Window display
        if full_screen:
            cv2.namedWindow("Pattern", cv2.WND_PROP_FULLSCREEN)
            cv2.setWindowProperty(
                "Pattern",
                cv2.WND_PROP_FULLSCREEN,
                cv2.WINDOW_FULLSCREEN)

        logging.info(
            f"Created projector of size ({self.display_width},{self.display_height})")

    def run(self, debug):
        """
        Blocking function
        Will run until a key is pressed or NONE is received
        """
        self.running = True
        self.__debug = debug
        self.loop_function()

    def get_pattern(self, received_list):
        pattern = np.zeros(
            (self.display_height, self.display_width, 3), dtype=np.uint8)
        pattern[:] = Utils.PROJECTION_BACKGROUND

        if "COORD" in received_list:
            points = received_list["COORD"]
            logging.info(f"Received coord {len(points)}")
            if len(points) == 4:
                self.rectangle_top_corner = (points[0], points[1])
                self.rectangle_bot_corner = (points[2], points[3])
        else:
            if self.__debug and self.rectangle_top_corner is not None:
                cv2.rectangle(
                    pattern,
                    self.rectangle_top_corner,
                    self.rectangle_bot_corner,
                    Utils.PROJECTION_COLOUR,
                    10)

            logging.debug(
                f"Received {len(received_list)} circles")
            Utils.draw_circles(
                pattern, received_list, Utils.PROJECTION_COLOUR, -1)
        return pattern

    def loop_function(self):
        logging.debug("Started loop")
        while self.running:
            try:
                projection_list = self.receive()
            except BaseException as e:
                logging.error(f"Exception while receiving {e}: continuing")
                continue
            try:
                if "NONE" in projection_list:
                    logging.info("Received NONE - stopping...")
                    break
                self.pattern[:] = self.get_pattern(projection_list)

            except BaseException as e:
                logging.error(f"Exception while drawing points {e}")
            self.transmit("PROJECTING")
            self.show_pattern()
            key = cv2.waitKey(33) & 0xFF
            if key == ord("q"):
                logging.info("'q' key pressed. Stopping...")
                break

        logging.debug("Stopped loop")
        self.transmit("NONE")

    def show_pattern(self):
        cv2.imshow("Pattern", self.pattern)

    def receive(self):
        data = self.serversocket.recv(1024)
        byteDecode = ""
        if not data:
            byteDecode = "NONE"
        else:
            byteDecode = json.loads(data)
        return byteDecode

    def transmit(self, transmit_message: str):
        msg = json.dumps(transmit_message)
        self.serversocket.send(bytes(msg, "utf-8"))
        return


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.StreamHandler(),
            logging.FileHandler("projector.log")
        ]
    )
    p = Projector('localhost', 65455)
    p.show_pattern()
    cv2.waitKey(0)
    p.run(debug=False)
