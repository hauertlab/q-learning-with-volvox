import json
import logging
import socket


class Communications():
    def __init__(self, addr, port):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
            # This line allows us to run again without waiting for the delay
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server_socket.bind((addr, port))
            server_socket.listen()
            self.clientsocket, addr = server_socket.accept()
            logging.info(f"Listening on {addr}:{port}")

    # send data to networked socket
    def transmit(self, transmitMessage):
        msg = json.dumps(transmitMessage)
        logging.debug(f"Transmitting {msg}")
        self.clientsocket.send(bytes(msg, "utf-8"))

    # send data to networked socket
    def recieve(self):
        data = self.clientsocket.recv(10000)
        # provide an exception for when no data is received to avoid json
        # expected value error
        if not data:
            byteDecode = "NONE"
        # otherwise load seralised data using json
        else:
            byteDecode = json.loads(data)
        return (byteDecode)
