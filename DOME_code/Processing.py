import logging
import time
import Utils
import volvox
from learning import Action, Behaviour
import enum
from matplotlib import cm
import math


class SwarmBehaviour(enum.Enum):
    PATTERN = 0
    HERD = 1
    LIGHT_HALF = 2
    STOP_ALL = 3
    LIGHT_ON_ALL = 4
    LIGHT_OFF_ALL = 5
    TRAIL = 6


class Processing():
    def __init__(self, img_shape=None, pattern_filename=None, write_to_file):
        self.__counter = 0
        self.__past_agents = {}
        self.__previous_time = time.time()
        self.__colourmap = cm.get_cmap('hsv', 50)
        self.__volvox = volvox.Volvox("detected_agents.csv", write_to_file)
        self.__swarm_behaviour = SwarmBehaviour.LIGHT_OFF_ALL
        # Value between 0 and 2*pi
        # 0 means going to the right, pi means going to the left
        # pi/2 means going up, 3pi/2 means going down
        self.__desired_direction = math.pi / 2
        self.__desired_pattern = None
        self.__trail_head_selected = False
        self.__count_episodes_all_stopped = 0
        if img_shape is not None and pattern_filename is not None:
            self.__desired_pattern = Utils.load_and_rescale(
                pattern_filename, img_shape)

    def _get_colour(self, id):
        # assume the largest value is around 700
        colour_float = self.__colourmap((float(id % 50) / 50))
        colour_rgb = (
            colour_float[0] * 255,
            colour_float[1] * 255,
            colour_float[2] * 255)
        return colour_rgb

    def end(self):
        self.__volvox.write_to_file(self.__past_agents)

    def set_behaviour(self, behaviour):
        self.__swarm_behaviour = behaviour
        self.__trail_head_selected = False

    def choose_action(self, agent, frame_shape):
        if self.__swarm_behaviour == SwarmBehaviour.LIGHT_ON_ALL:
            return agent.predict_action(Behaviour.LIGHT_ON_ALWAYS)
        elif self.__swarm_behaviour == SwarmBehaviour.LIGHT_OFF_ALL:
            return agent.predict_action(Behaviour.LIGHT_OFF_ALWAYS)
        elif self.__swarm_behaviour == SwarmBehaviour.PATTERN:
            if self.__desired_pattern is not None:
                row_index = int(agent.position().y())
                col_index = int(agent.position().x())
                if 0 <= row_index < self.__desired_pattern.shape[
                        0] and 0 <= col_index < self.__desired_pattern.shape[1]:
                    if self.__desired_pattern[row_index, col_index] == 0:
                        return agent.predict_action(Behaviour.STOP)

                return Action.LIGHT_OFF

        elif self.__swarm_behaviour == SwarmBehaviour.LIGHT_HALF:
            if (agent.position().x() >= frame_shape[1] // 2):
                return agent.predict_action(Behaviour.LIGHT_ON_ALWAYS)
            else:
                return agent.predict_action(Behaviour.LIGHT_OFF_ALWAYS)
        elif self.__swarm_behaviour == SwarmBehaviour.STOP_ALL:
            return agent.predict_action(Behaviour.LEARN_STOP)
        elif self.__swarm_behaviour == SwarmBehaviour.HERD:
            return agent.predict_action(
                Behaviour.LEARN_DIRECTION, self.__desired_direction)
        elif self.__swarm_behaviour == SwarmBehaviour.TRAIL:
            return agent.predict_action(Behaviour.TRAIL)
        else:
            return Action.LIGHT_ON

    def agent_location(self, img):
        # get list of image detections in the current frame
        img_detections = self.__volvox.find_agents(img)
        matched_agents_dict = {}
        # check if any agents are in FOV
        if len(img_detections) == 0:
            logging.error("No agents detected this frame")
            return []
        # for first instance of loop, initialise time zero
        if self.__counter == 0:
            self.__previous_time = time.time()
        # for all other loop instances, match agents and calculate velocity or
        # displacement
        else:
            current_time = time.time()
            time_difference = (current_time - self.__previous_time) * 1000
            matched_agents_dict = self.__volvox.match_detections(
                self.__past_agents, img_detections, time_difference, self.__counter)
            self.__previous_time = current_time

        control_agents = []
        # check if agents are active, both in the individual frame and in
        # longer term tracking
        if self.__swarm_behaviour == SwarmBehaviour.TRAIL and not self.__trail_head_selected:
            if len(matched_agents_dict) > 0:
                agent_key = next(iter(matched_agents_dict))
                matched_agents_dict[agent_key].emit(True)
                matched_agents_dict[agent_key].add_to_trail()
                self.__trail_head_selected = True

        if self.__counter != 0:
            if self.__swarm_behaviour == SwarmBehaviour.TRAIL:
                # Extra stuff needed for trailing algorithm
                emission_range = 50
                max_distance = 0
                max_agent = None
                for agent_id, agent in matched_agents_dict.items():
                    if agent.is_emitting():
                        agents_affected = 0
                        for agent_id_2, agent_2 in matched_agents_dict.items():
                            if agent_id == agent_id_2:
                                continue
                            if not agent_2.is_in_trail():
                                agent_distance = agent.distance(agent_2)
                                agent_direction = agent.position().direction(agent_2.position())
                                if agent_distance < emission_range:
                                    agents_affected += 1
                                    agent_2.add_to_trail()
                                    if max_agent is None or agent_distance > max_distance:
                                        max_distance = agent_distance
                                        max_agent = agent_2

                        if agents_affected > 0:
                            agent.emit(False)
                        if max_agent is not None:
                            max_agent.emit(True)

            for agent_id, agent in matched_agents_dict.items():
                # check we have at least 1 previous entry
                if self.__swarm_behaviour == SwarmBehaviour.TRAIL:
                    colour = (
                        255, 0, 0) if agent.is_in_trail() else (
                        0, 255, 255)
                    if agent.is_emitting():
                        colour = (0, 255, 0)
                else:
                    colour = (
                        255, 255, 0) if agent.has_learned() else (
                        0, 0, 255)
                if agent.detected():
                    if self.choose_action(agent, img.shape) == Action.LIGHT_ON:
                        control_agents.append(agent.to_list())

        self.__past_agents = matched_agents_dict

        return control_agents

    def process_frame(self, img):
        locations = self.agent_location(img)
        self.__counter += 1
        return locations
